g = 9.82;           %gravity, kgm/s²
m = 0.057;          %mass of system, kg
l = 1.01;              %rod length, m
Kmotor = 0.067;

alpha45 = [0.037784 0.017803 0.005393 0.000195 0.002062 0.011047 0.027406 0.051617];
omega_m45 = [13 15.2 19.1 35 23 16.7 13.9 12.2];

alpha85 = [0.025086 0.065924 0.129616 0.221431 0.349752 0.527864 0.777251 1.133995];
omega_m85 = [14.1 11.6 10 8.91 8.05 7.34 6.72 6.15];

alpha35 = [0.082223 0.054153 0.032374 0.01641 0.005921 0.000686 0.000597 0.005654];
omega_m35 = [11 11.5 13.4 15.3 18.7 27.9 28.6 19];

alpha55 = [0.011047 0.001345 0.000988 0.009963 0.028607 0.057638 0.098208 0.152009];
omega_m55 = [16.7 24.8 26.3 17 13.8 22.9 10.7 9.68];

%% UNCOMPENSATED
Kp = 1;
%alpha = 0.0019;
%omega_m = 2.25;     %rad/s

tau_dn = 0;%1/(omega_m*sqrt(alpha))
tau_dd = 0;%tau_dn*alpha

[A, B, C, D] = linmod('Model_02');
[num, den] = ss2tf(A,B,C,D);
G = tf(num, den)
sysU = ss(A,B,C,D);

%% COMPENSATED 1.0
Kp      = 1;
alpha   = alpha55(1);
%Am      = 15.29;    %dB
omega_m = omega_m55(1);     %rad/s

tau_dn = 1/(omega_m*sqrt(alpha));
tau_dd = tau_dn*alpha;

[A, B, C, D] = linmod('Model_02');
[num, den] = ss2tf(A,B,C,D);
G = tf(num, den);
sys1 = ss(A,B,C,D);

%% COMPENSATED 1.1
Kp      = 1;
alpha   = alpha55(2);
%Am      = 15.29;    %dB
omega_m = omega_m55(2);     %rad/s

tau_dn = 1/(omega_m*sqrt(alpha));
tau_dd = tau_dn*alpha;

[A, B, C, D] = linmod('Model_02');
[num, den] = ss2tf(A,B,C,D);
G = tf(num, den);
sys11 = ss(A,B,C,D);

%% COMPENSATED 1.2
Kp      = 1;
alpha   = alpha45(3);
%Am      = 15.29;    %dB
omega_m = omega_m45(3);     %rad/s

tau_dn = 1/(omega_m*sqrt(alpha));
tau_dd = tau_dn*alpha;

[A, B, C, D] = linmod('Model_02');
[num, den] = ss2tf(A,B,C,D);
G = tf(num, den);
sys12 = ss(A,B,C,D);

%% COMPENSATED 1.3
%Følgende virker rigtig godt, indsvingningstid = 0.8s
%Kp      = 100;%
%alpha   = alpha45(4)*10; %=0.00195
%Am      = 15.29;    %dB
%omega_m = 70;     %rad/s 15

%Følgende virker også godt med indsvingstid ≃ 0.98s
Kp      = 70;
alpha   = 0.025256;
omega_m = 15;
tau_dn = 1/(omega_m*sqrt(alpha));
tau_dd = tau_dn*alpha;


numG = [tau_dn 1];
denG = [tau_dd 1];
G_C = Kp*tf(numG,denG);

[A, B, C, D] = linmod('Model_02');
[num, den] = ss2tf(A,B,C,D);
G = tf(num, den);
sys13 = ss(A,B,C,D);

%% BILINEAR TRANSFORM

[numd, dend] = bilinear(numG, denG, 1000);
70*filt(numd, dend,1/1000)
dn = 1;
dd = 1;

tau_dnz = 0;
tau_ddz = 0;

%% COMPENSATED 1.4
Kp      = 2;
alpha   = alpha55(5);
%Am      = 15.29;    %dB
omega_m = omega_m55(5);    %rad/s

tau_dn = 1/(omega_m*sqrt(alpha));
tau_dd = tau_dn*alpha;

[A, B, C, D] = linmod('Model_02');
[num, den] = ss2tf(A,B,C,D);
G = tf(num, den);
sys14 = ss(A,B,C,D);

%% COMPENSATED 1.5
Kp      = 1;
alpha   = alpha55(6);
%Am      = 15.29;    %dB
omega_m = omega_m55(6);     %rad/s

tau_dn = 1/(omega_m*sqrt(alpha));
tau_dd = tau_dn*alpha;

[A, B, C, D] = linmod('Model_02');
[num, den] = ss2tf(A,B,C,D);
G = tf(num, den);
sys15 = ss(A,B,C,D);

%% COMPENSATED 1.6
Kp      = 1;
alpha   = alpha55(7);
%Am      = 15.29;    %dB
omega_m = omega_m55(7);    %rad/s

tau_dn = 1/(omega_m*sqrt(alpha));
tau_dd = tau_dn*alpha;

[A, B, C, D] = linmod('Model_02');
[num, den] = ss2tf(A,B,C,D);
G = tf(num, den);
sys16 = ss(A,B,C,D);

%% COMPENSATED 1.7
Kp      = 1;
alpha   = alpha55(8);
%Am      = 15.29;    %dB
omega_m = omega_m55(8);     %rad/s

tau_dn = 1/(omega_m*sqrt(alpha));
tau_dd = tau_dn*alpha;

[A, B, C, D] = linmod('Model_02');
[num, den] = ss2tf(A,B,C,D);
G = tf(num, den);
sys17 = ss(A,B,C,D);
%%

figure(3);
W=logspace(-1,3,3000);
bode(sysU,W,'y');
hold on;
bode(sys1,W,'m');
bode(sys11,W,'c');
bode(sys12,W,'r');
bode(sys13,W,'g');
bode(sys14,W,'b');
bode(sys15,W,'k');
bode(sys16,W,'r-.');
bode(sys17,W,'b-.');
legend('Uncomp.','Comp. 1.0','Comp. 1.1','Comp. 1.2','Comp. 1.3','Comp. 1.4','Comp. 1.5','Comp. 1.6','Comp. 1.7');
hold off;
%figure;
Gm = size(9);
Pm = size(9);
Wgm = size(9);
Wpm = size(9);
[Gm(1),Pm(1),Wgm(1),Wpm(1)] = margin(sysU);
[Gm(2),Pm(2),Wgm(2),Wpm(2)] = margin(sys1);
[Gm(3),Pm(3),Wgm(3),Wpm(3)] = margin(sys11);
[Gm(4),Pm(4),Wgm(4),Wpm(4)] = margin(sys12);
[Gm(5),Pm(5),Wgm(5),Wpm(5)] = margin(sys13);
[Gm(6),Pm(6),Wgm(6),Wpm(6)] = margin(sys14);
[Gm(7),Pm(7),Wgm(7),Wpm(7)] = margin(sys15);
[Gm(8),Pm(8),Wgm(8),Wpm(8)] = margin(sys16);
[Gm(9),Pm(9),Wgm(9),Wpm(9)] = margin(sys17);

figure;
subplot(2,1,1)
plot(Gm)
title('Gain margin [dB]')
subplot(2,1,2)
plot(Pm)
title('Phase margin deg')
% hold on;
% margin(sys2);
% hold off;