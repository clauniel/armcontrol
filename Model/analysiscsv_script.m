load analysis1.csv;
x = analysis1(:,1)-analysis1(1,1);
u = analysis1(:,2);%*10/2^(15);
output = analysis1(:,3);
x_rad = analysis1(:,4);
data = analysis1(:,5);
ref_deg = analysis1(:,6);
accZ = analysis1(:,7);
aout = analysis1(:,8);
pout = analysis1(:,9);
pot = analysis1(:,10);
pd = analysis1(:,11);
fs=200;
%%
load analysis1.csv;
x = analysis1(:,1)-analysis1(1,1);
pot = analysis1(:,2);
pd = analysis1(:,3);
a = analysis1(:,4);
pout = analysis1(:,5);
aout = analysis1(:,6);
u = analysis1(:,7);
%%
%plot(x, pd)
plot(x, (37691.0*(pd-32000).^(-1.338)-0.2427)/0.0183,x,180/pi*(pot-32000-6176)*0.00018338)
%plot(x, (37691.0*(pd-32000).^(-1.338)+12.733)/57.934,x,180/pi*(pot-32000-6176)*0.00018338)
%plot(x, 57.934*(37691.0*(pd-32000).^(-1.338))-12.733,x,180/pi*(pot-32000-6176)*0.00018338)
legend('pd','pot')
%%
figure(2)
subplot(4,1,1)
plot(x,pd./pot)
subplot(4,1,2)
plot(x,180/pi*(pd),x,a,x,180/pi*pot,x,abs((pot-pd)),x,10)
legend('pd','a','pot','pot-pd')
subplot(4,1,3)
plot(x,aout,x,pout,x,pot,x,aout+pout)
legend('aout','pout','pot','Comp.filt.')
subplot(4,1,4)
plot(x,pot,x,pout+aout,x,pd)
legend('pot','Comp.filt','pd')
%%
y=[x pot pd a pout aout u];
csvwrite('n_close_10Hz_test_pi2.csv',y);
%% Potmeter measurements
figure(1)
subplot(2,1,1)
plot(x, data)
subplot(2,1,2)
%plot(x,180/pi*(data-6322)*0.00019306)
plot(x,180/pi*(data-6033)*0.00018338)

%figure(1)  
%avg=filterdata(data,80);
%plot(x(1:fs*2.5), data(1:fs*2.5),x(1:fs*2.5),data1(1:fs*2.5),x2(1:fs*2.5),data2(1:fs*2.5));
%legend('No cap.','10µF Vcc','10µF Vcc and 220nF Vo');

%plot(x, data/1, x, u);
%legend('data','u');

%plot(x, aout+pout,x,(155451*data.^(-1.447)-0.7635)/0.0191, x, aout, x, pout)
%legend('aout+pout','angle','aout','pout')
%plot(x,aout+pout)
%axis([0,3,0,11000]);
%% Distance sensor measurements
figure(1)
%plot(x,(155451*data.^(-1.447)-0.7635)/0.0191)
%plot(x,(350257*data.^(-1.61)-0.7635)/0.0191)
%plot(x, (37691.0*data.^(-1.338)-0.2225)/0.0182);
plot(x,pd)
%plot(x, data)
%t = sprintf('Mean= %.4f, stdev= %0.4f,max= %f, min= %f',mean(data),std(data),max(data),min(data));
%title(t);


%% Distance and accelerometer
figure(1)
subplot(3,2,1)
plot(x,data)
subplot(3,2,2)
%plot(x, pi/180*(155451*data.^(-1.447)-0.7635)/0.0191,x,pout)
plot(x, pi/180*(350257.0*data.^(-1.61)-0.7635)/0.0191,x, pout)
%plot(x, pi/180*(37691.0*data.^(-1.338)-0.2225)/0.0182,x,pout)
legend('distance','pout')
subplot(3,2,3)
plot(x,accZ)
subplot(3,2,4)
plot(x,((accZ)./16832-1)/9.82,x,aout)
legend('accZ','aout')
subplot(3,2,5)
plot(x, aout,x,pout,x,aout+pout,x,ref_deg)
legend('aout','pout','filter out')
subplot(3,2,6)
plot(x,ref_deg*180/pi,x,(aout+pout)*180/pi)
%%
y=[x data u ref_deg x_rad accZ];
csvwrite('PLead_70_08__80_10Hz.csv',y);
%%
load 'Close_PLead_70_10__100.csv'
xf = Close_PLead_70_10__100(:,1);
df = Close_PLead_70_10__100(:,2);
plot(xf,(37691.0*df.^(-1.338)-0.2225)/0.0182)
%%
load 'Resolution_close.csv';
xf = Resolution_close(:,1);
df = Resolution_close(:,2);
%plot(xf,(350257*df.^(-1.61)-0.7635)/0.0191)
plot(xf, (37691.0*df.^(-1.338)-0.2225)/0.0182)
%%

load 'Filter.csv';
load 'Nofilter.csv';
load 'Filter_motor_on.csv';
xf = Filter(:,1);
df = Filter(:,2);
xn = Nofilter(:,1);
dn = Nofilter(:,2);
xm = Filter_motor_on(:,1);
dm = Filter_motor_on(:,2);
plot(xf(1:800),df(1:800),xn(1:800),dn(1:800),xm(1:800),dm(1:800))
legend('Filter','No filter', 'Filter and motor on')
%%
y=[x data u ref_deg-x_rad];
csvwrite('PILead_70_5_015_100_n1.csv',y);
%%

load PILead_70_10_01_100.csv;
x1 = PILead_70_10_01_100(:,1);
data1 = (155451*PILead_70_10_01_100(:,2).^(-1.447)-0.7635)/0.0191;
load PILead_70_10_02_100.csv;
x2 = PILead_70_10_02_100(:,1);
data2 = (155451*PILead_70_10_02_100(:,2).^(-1.447)-0.7635)/0.0191;
load PILead_70_10_03_100.csv;
x3 = PILead_70_10_03_100(:,1);
data3 = (155451*PILead_70_10_03_100(:,2).^(-1.447)-0.7635)/0.0191;
n = 15;
figure(2)
plot(x1(1:n*fs), data1(1:n*fs), x2(1:n*fs), data2(1:n*fs),x3(1:n*fs),data3(1:n*fs));
legend('0.1','0.2','0.3')

% plot(x(1:n*fs), (155451*data(1:n*fs).^(-1.447)-0.7635)/0.0191,x2(1:n*fs),data2(1:n*fs));
%axis([0,3,0,11000]);
set(refline([0 10]),'color','c')
%% Rolling average
figure(2)
%m = mean(data(18*fs:21.5*fs))
%v = var(x_rad(2.5*fs:end)*180/pi)
%t = sprintf('Mean= %.4f, Var= %.4f, offset=115.5',m,v);


%plot(x,u,x,output,x,x_rad*180/pi,x,data,x,ref_deg*180/pi);
%legend('u','output','x_deg','data','ref_deg');
%plot(x,x_rad*180/pi,x,ref_deg*180/pi);
%legend('x_deg','ref_deg');
%title(t)
subplot(2,1,1)
plot(x,data);
mu = mean(data);
hline = refline([0 mu]);
set(hline, 'color','g');
t = sprintf('Mean= %.4f',mu);
title(t);

% axis([0 2.5 3600 4800]);



%avg = zeros(length(data)-19,2);
% for i=10:length(data)
%     sum = 0;
%     D = data(i);
%     for j=(i-9):i;
%         if(j>1)
%             if (data(j) > D+100)
%                 
%             else
%                 D=data(j);
%             end
%         end
%         
%        sum = sum + D;
%     end
%     
%     avg(i-9,:) = [x(i) sum/10];
% end

 windowSize = 20;
% for i=2:length(data1)
%     if (data1(i)>data1(i-1)*1.05 || data1(i)<data1(i-1)*0.95)
%        data1(i)=data1(i-1); 
%     end
% end

avg = filter(ones(1,windowSize)/windowSize,1,data);
subplot(2,1,2)
plot(x,data,x,avg);
legend('data','avg');
mu = mean(data);
hline = refline([0 mu]);
set(hline, 'color','g');
set(refline([0 mean(datamod)*1.01]),'color','r');
set(refline([0 mean(datamod)*0.99]),'color','c');
t = sprintf('Mean= %.4f, stdev= %0.4f,max= %f, min= %f',mu,std(data),max(data),min(data));
title(t);
% axis([0 2.5 3600 4800]);

%% Pulling out values
xmod = x;
datamod = data;
i = 1;
samples = [0 0 0 0 0 0 0 0 0 0];
nsamples = 0;
l_samp = length(samples);
sum=0;
gns=0;

endpoint = length(datamod);

while(i<=length(datamod))
    sum=0;
    gns=0;
    
    if(nsamples < l_samp)
        samples(nsamples+1)=datamod(i);
        nsamples=nsamples+1;
        
    else
        for j=1:nsamples-1
            samples(j)=samples(j+1);
        end
        samples(l_samp)=datamod(i);
    end
    
    
    for j=1:nsamples
        sum = sum + samples(j);
    end
    
    
    gns = sum/nsamples;
    
    
    if (nsamples == l_samp)
        j=1;
        while (j<=nsamples)
            if (j==0)
                
            elseif((samples(j) > gns*1.01) || (samples(j) < gns*0.99))
                datamod(i-(nsamples-j)) = [];
                xmod(i-(nsamples-j)) = [];
                i=i-1;
                samples(nsamples-j+1) = [];
                j=j-1;
                nsamples=nsamples-1;
            end
            j=j+1;
        end
    end
    
    
    i=i+1;
end

figure(3)
plot(xmod,datamod)
t = sprintf('Mean = %.2f, dev = %.2f, max = %f, min = %f',mean(datamod),...
    std(datamod), max(datamod), min(datamod));
%hline = refline([0 mean(datamod)]);
%set(hline, 'color','g');
%set(refline([m mean(datamod)*1.01]),'color','r');
%set(refline([m mean(datamod)*0.99]),'color','p');
title(t);
axis([0 2.5 3700 4000]);

%%
figure(3)
plot(x,data,x,filterdata(data,10),x,filterdata(data,20),x,filterdata(data,40),x,filterdata(data,80))
%plot(x,data,x,filterdata(data,10))
legend('data','Filtered, 10', 'Filtered, 20','Filtered, 40', 'Filtered, 80');
t = sprintf('Mean = %.2f, dev = %.2f, max = %f, min = %f',mean(outp), std(outp), max(outp), min(outp));
%hline = refline([0 mean(datamod)]);
%set(hline, 'color','g');
%set(refline([0 mean(datamod)*1.01]),'color','r');
%set(refline([0 mean(datamod)*0.99]),'color','m');
title(t);
%axis([0 2.5 3800 4025]);

% Convert to angle
h = (outp).^(-1.525).*307092;
angle = h.*57.934-43.786;
figure(4)
plot(x,angle)
axis([0 2.5 15 17])