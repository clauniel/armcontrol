g = 9.82;           %gravity, kgm/s²
m = 0.057;          %mass of system, kg
l = 1.01;              %rod length, m
Kmotor = 0.067;

%% UNCOMPENSATED
Kp = 1;
%alpha = 0.0019;
%omega_m = 2.25;     %rad/s

tau_dn = 0;%1/(omega_m*sqrt(alpha))
tau_dd = 0;%tau_dn*alpha

[A, B, C, D] = linmod('Model_02');
[num, den] = ss2tf(A,B,C,D);
G = tf(num, den)
sysU = ss(A,B,C,D);

%% COMPENSATED
%Følgende virker rigtig godt, indsvingningstid = 0.8s
%Kp      = 100;%
%alpha   = alpha45(4)*10; %=0.00195
%Am      = 15.29;    %dB
%omega_m = 70;     %rad/s 15

%Følgende virker også godt med indsvingstid ≃ 0.98s
Kp      = 70;
alpha   = 0.025256;
omega_m = 15;
tau_dn = 1/(omega_m*sqrt(alpha));
tau_dd = tau_dn*alpha;


numG = [tau_dn 1];
denG = [tau_dd 1];
G_C = Kp*tf(numG,denG);

[A, B, C, D] = linmod('Model_02');
[num, den] = ss2tf(A,B,C,D);
G = tf(num, den);
sysC = ss(A,B,C,D);

%% BILINEAR TRANSFORM

[numd, dend] = bilinear(numG, denG, 200);
G_D = Kp*filt(numd, dend,1/200)



%%

figure();
W=logspace(-1,3,3000);
%bode(sysU,W,'y');
bode(sysU,W);
hold on
bode(sysC,W,'r');
hold off