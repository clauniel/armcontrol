function [ outp ] = filterdata( data, l_samp )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

outp = zeros(length(data),1);
i = 1;
nsamples = 0;
%l_samp = 50;
samples = zeros(l_samp,1);

while(i<=length(data))
    sum=0;
    gns=0;
    
    if(nsamples < l_samp)
        samples(nsamples+1)=data(i);
        nsamples=nsamples+1;
        
    else
        for j=1:nsamples-1
            samples(j)=samples(j+1);
        end
        samples(l_samp)=data(i);
    end
    
    
    for j=1:nsamples
        sum = sum + samples(j);
    end
    
    
    gns = sum/nsamples;
    sum = 0;
    
    forget = 0;
    
    j=1;
    while (j<=nsamples)               
        if((samples(j) > gns*1.005) || (samples(j) < gns*0.995))
            forget=forget+1;
        else
            sum = sum + samples(j); 
        end
        j=j+1;
    end
    
    outp(i) = sum/(nsamples-forget);
    
    i=i+1;
end

end

