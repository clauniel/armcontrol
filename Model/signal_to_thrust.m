function [ T ] = signal_to_thrust( omega )
%signal_to_thrust turns a digital signal into its equivalent thrust
%   u being signal and T being thrust

T = 1.55E-5*(omega).^2;

end

