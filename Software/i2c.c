/***********************************
* i2c library using hardware TWI interface 
***********************************/

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <compat/twi.h>

// CPU running at 16MHz
#define F_CPU 16000000UL 

// SCL frequency is 100kHz
#define SCL_CLOCK 100000L

// TWSR values
#define TW_START 0x08
#define TW_REP_START 0x10

// Master transmitter
#define TW_MT_SLA_ACK 0x18

// Master receiver
#define TW_MR_SLA_ACK 0x40

#define TW_STATUS_MASK ((1<<TWS7)|(1<<TWS6)|(1<<TWS5)|(1<<TWS4)|(1<<TWS3))
#define TW_STATUS (TWSR & TW_STATUS_MASK)

void i2c_init(void)
{
  // No prescaler
  TWSR = 0;
  TWBR = ((F_CPU/SCL_CLOCK)-16)/2;
}


/*****************************
  Issues a start condition and sends address and transfer direction.
  return 0 = device accessible, 1= failed to access device
*****************************/
unsigned char i2c_start(unsigned char address)
{
  uint8_t twst;
  
  // Send START condition
  TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN);
  
  // Wait until transmission completed
  while(!(TWCR & (1<<TWINT)));
  
  // Check value of TWI Status Register and mask prescaler bits
  twst = TWSR & 0xF8;
  if ((twst != TW_START) && (twst != TW_REP_START)) return 1;
  
  // Send device address
  TWDR = address;
  TWCR = (1<<TWINT) | (1<<TWEN);
  
  // Wait until transmission is completed and ACK/NACK has been received
  while(!(TWCR & (1<<TWINT)));
  
  // Check value of TWI Status Register and mask prescaler bits
  twst = TW_STATUS & 0xF8;
  if ((twst != TW_MT_SLA_ACK) && (twst != TW_MR_SLA_ACK)) return 1;
  
  return 0;
}