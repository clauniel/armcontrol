load analysis1.csv;
x = analysis1(:,1)-analysis1(1,1);
pot = analysis1(:,2);
pd = analysis1(:,3);
a = ((analysis1(:,4)+700)./16832-1)*9.82;
a = analysis1(:,4);
pout = analysis1(:,5);
aout = analysis1(:,6);
u = analysis1(:,7);

V = zeros(length(x),1);
V1 = 0;
dt = 0.005;

Pa = zeros(length(x),1);
P1 = 0;

for i = 2:length(x)
   V(i) = V1 + dt*a(i-1);
   V1 = V(i);
end

for i = 2:length(x)
   Pa(i) = P1 + dt*V(i-1); 
end

figure(1)
subplot(2,1,1)
plot(x,pot*180/pi,x,Pa*180/pi)
legend('pot','P_a')
subplot(2,1,2)
plot(x,a)
legend('a')