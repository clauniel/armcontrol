#define DEBUG
#include "pack.h"

void bitUnpack( unsigned char* bitOut8, unsigned char* bitIn7, int n, int unpackSize );
int findPackets( int n, int packetSize, unsigned char * recBytes, unsigned char * recPackets );

static int numBytesBuffered;

/* unpackPackets :
 * Unpacks a sequence of packages in the format |1xxxxxxx|0xxxxxxx|......|0xxxxxxx|
 * Non-whole packets are thrown away. A non-whole packet ending a message is buffered an
 * prepended to the next call. 
 * packetSize: The size of a packet in bytes.
 * packedData: The data to be unpacked.
 * numBytesIn: Size of packedData in bytes.
 * unpackedData: A pointer to the location where data will be unpacked.
 * return: Size of unpacked data in bytes.
 */
int unpackPackets( int packetSize, unsigned char * packedData, int numBytesIn, unsigned char * unpackedData ) {
	unsigned char * recPackets = malloc( sizeof( unsigned char ) * ( numBytesIn + packetSize ) );
	#ifdef DEBUG
	int i;
	printf( "numBytesIn: %d\n", numBytesIn );
	printf( "full: " );
	for ( i = 0; i<200; i++ )
		printf( "%3d, ", packedData[ i ] );
	printf( "\n" );
	#endif
	int numPackets = findPackets( numBytesIn, packetSize, packedData, recPackets );
	#ifdef DEBUG
	printf( "numPackets: %d\n", numPackets );
	printf( "extr: " );
	for ( i = 0; i<numPackets*packetSize; i++ )
		printf( "%3d, ", recPackets[ i ] );
	printf( "\n" );
	#endif
	//unpacksize = n_packs * floor( pack_size_packed*7/8 )
	int unpackSize = numPackets * ( ( packetSize * 7 ) / 8 );
	bitUnpack( unpackedData, recPackets, packetSize, unpackSize );

	free( recPackets );
	return unpackSize;
}

/* bitUnpack:
 * Does the actual unpacking of the data.
 * bitOut8: Location where data will be written.
 * bitIn7: Data to unpack.
 * packetSize: size of packet.
 * unpackSize: Size of bitOut8 in bytes.
 */
void bitUnpack( unsigned char* bitOut8, unsigned char* bitIn7, int packetSize, int unpackSize ) {
	int n,j,a,b;
	int i = 1;
	int off = 0;
	int unpackedPacketSize = (packetSize*7)/8;
	for ( n = 1; n <= unpackSize; n++ ) {
		*(bitIn7 + n - 1) &=0x7f;
		j = ( 8*( i - 1 ) ) / 7 + 1;
		a = 1 + ( i - 1 ) % 7;
		b = -6 + ( i - 1 ) % 7;
		*(bitOut8 + n - 1) = ( *(bitIn7 + j - 1 + off * packetSize ) << a ) | ( *(bitIn7 + j + off * packetSize ) >> -b );
		i++;
		if ( i > unpackedPacketSize ) { //Restart sequence after each decoded packet. (fenceposting)
			i = 1;
			off++;
		}
	}
}

/* findPackets: 
 * Finds whole packets in a message and discard incomplete packets. If the message ends with an 
 * incomplete packet, that packet is buffered and prepended to the next message. 
 * n: Number of bytes in message.
 * packetSize: Size of a packet in bytes.
 * recBytes: Pointer to the recieved message.
 * recPackets: Pointer to write location for found packets
 */
int findPackets( int n, int packetSize, unsigned char * recBytes, unsigned char * recPackets ) {
	unsigned char temp[packetSize];
	bool completepacket = true;
	int shortPacket = 0;
	int packetCount = 0;
	int i,j;
	// something is wrong here!
	for ( i = packetSize - numBytesBuffered; i < n + packetSize; i++ ) {
		numBytesBuffered = 0;
		shortPacket = 0;
		completepacket = true;
		if ( ( ( *(recBytes + i ) & 0x80 ) > 0 )  && ( i + packetSize ) <= ( n + packetSize ) ) {
			temp[ 0 ] = *(recBytes + i );
			for ( j = 1; j < packetSize; j++ ) {
				temp[ j ] = *(recBytes + i + j );
				if ( (temp[ j ] & 0x80) > 0 ) {
					completepacket = false;
					shortPacket = j;
					break;
				}
			}
			if ( completepacket == true ) {
				for ( j = 0; j < packetSize; j++ ) {
					*(recPackets+j + packetCount * packetSize) = temp[ j ];
				}
				packetCount++;
				i += ( packetSize - 1 ); // i++ happens after loop exit
			} else {
				i += shortPacket;
			}
		} else if ( ( i + packetSize ) > ( n + packetSize ) ) {
			numBytesBuffered = n + packetSize - i;
			for ( j = 0; j < numBytesBuffered; j++ ) {
				*( recBytes + packetSize - numBytesBuffered + j ) = *( recBytes + j + i );
			}
			break;
		}

	}

	return packetCount;
}
