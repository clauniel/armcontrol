
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/io.h>
#include <sched.h>
#include <fcntl.h>
#define KEEP_STATIC_INLINE
#include <pthread.h>
#include <rtai_lxrt.h>
#include <rtai_comedi.h>
#include <rtai_sched.h>
//#include <comedilib.h>
//derp

#include <math.h>

#define DEBUG
#include "util/globalfunc.h"
#include "util/encode.h"
#include "hexacontrol_v1.h"
#include "util/pack.h"
#include "util/estimator.h"


#define SOFT_RT


#define MILLISEC(x) (1000000 * (x)) /* ms -> ns */
#define PERIOD      MILLISEC(5) /*100 33 5 */
#define NULDEG 6033/*6176 /6322 /Value of input at 0 degrees*/
#define RADPERQUANT 0.0001798/*0.00018338 /0.00019306 /rads per quant*/
/* How often the hexacopter debug subscription is renewed.
   Must be done at least every 4 seconds */
#define DEBUG_REQUEST_PERIOD  3000
// txDeBuffer holds the non-encoded (decoded) data to be transmittee to the Hexacopter
#define MK_MAX_TX_DE_BUFFER 170
// rxEnBuffer holds the encoded data recieved from the Hexacopter
#define MK_MAX_RX_EN_BUFFER 170
// rxDeBuffer holds the decoded data recieved from the Hexacopter
#define MK_MAX_RX_DE_BUFFER 170

#define PACKED_SIZE 16
#define UNPACKED_SIZE 14

#define ULTRASOUND //define either ULTRASOUND, POTMETER or IRSENSOR for control mode

// The following is for use with the potmeter
// #########################
// 10 VOLT ON POTENTIOMETER, 16 BIT ADC
// 0 deg ~= 6322
// 90 deg ~= 14459
// (pi/2)/(14459-6322) = 0.00019306 rad/kvant

/* 0  deg ~=  6033
 * 90 deg ~= 14780
 * (pi/2)/(14780-6033) = 0.00017958 rad/kvant
 */

///////////////////////////////////////
// Serial related methods
///////////////////////////////////////
int initSerial(void);
int OpenAndSetMode(void);
int setupSerial(void);
void * Hexacopter(void * not_used);

struct {
	/** is receive thread running */
	int running;

	/** thread handle and attributes */
	pthread_t serial_task;
} rxtask;

typedef struct {
	double e,u,e1,e2,u1,u2,u3,offset;
	double x_rad;
	double in1,in2,in3,out1,out2;
} PILeadController_t;

typedef struct {
	double ain1,ain2,aout1,aout2;
	double pin1,pin2,pout1,pout2;

	double comp, comp1;
	double aout, pout;
} complementaryFilter_t;

typedef struct {
	int accX;
	int accY;
	int accZ;
	double angAccZ; //angular acc
} acceleration_t;

struct str_DebugOut debugOut;
struct str_ExternControl externCtrl;

static int running=1;
static int shutdown=0;

const char* control_file = "control_log_sys_step_log_fast_ramp_.txt";
const char* debug_file = "debug_log_sys_step_log_fast_ramp_.txt";
// Controller constants    
const double Kpos = 160;
const double b0 = 10;
const double b1 = -9.426; 
const double b2 = 0;
const double a1 = -0.4258; 
const double a2 = 0;
const char * filename = "analysis1.csv";

//Reference variables
double ref_pos=0.1;//10*3.1415/180.0;

//program status
int run_program;

//teensy-bound output
int output = 0;

//Comm buffers
unsigned char txDeBuffer[MK_MAX_TX_DE_BUFFER];
unsigned char rxEnBuffer[MK_MAX_RX_EN_BUFFER];
unsigned char rxDeBuffer[MK_MAX_RX_DE_BUFFER];

// File handling
char buffer[100];
FILE * ft;
int lines = 0;


int millisecToTicks(int millisec) {
	return millisec/(int)(PERIOD/1000000);
}

void initController(PILeadController_t *cont ) {
	cont->e = 0;
	cont->e1 = 0;
	cont->e2 = 0;
	cont->u = 0;
	cont->u1 = 0;
	cont->u2 = 0;
	cont->u3 = 0;

	cont->in1 = 0;
	cont->in2 = 0;
	cont->in3 = 0;
	cont->out1 = 0;
	cont->out2 = 0;
	cont->offset = 142;//<- GP2Y0A02YK
}

void initFilter( complementaryFilter_t *filter ) {
	//Daccfilt
	filter->ain1 = 0;
	filter->ain2 = 0;
	filter->aout1 = 0;
	filter->aout2 = 0;
	//Dposfilt
	filter->pin1 = 0;
	filter->pin2 = 0;
	filter->pout1 = 0;
	filter->pout2 = 0;

	filter->comp = 0;
	filter->comp1 = 0;
}

void initAcc( acceleration_t *acc ) {
	acc->accX = 0;
	acc->accY = 0;
	acc->accZ = 0;
	acc->angAccZ = 0;
}


void updateFilter( complementaryFilter_t *filter, acceleration_t *acc, double pot ) {
	// Complementary filter
	// Made using Matlabs c2d for 200Hz
	// w_c = 20
	filter->aout = 0.000011697*filter->ain1 + 0.000010953*filter->ain2 + 1.8097*filter->aout1 - 0.81873*filter->aout2;
	filter->pout = 0.185645*filter->pin1 +- 0.17659*filter->pin2 + 1.8097*filter->pout1 - 0.81873*filter->pout2;


	filter->ain2 = filter->ain1;
	filter->ain1 = acc->angAccZ;
	filter->aout2 = filter->aout1;
	filter->aout1 = filter->aout;

	filter->pin2 = filter->pin1;
	filter->pin1 = pot;
	filter->pout2 = filter->pout1;
	filter->pout1 = filter->pout;
}

void updateController( PILeadController_t *controller, estimator_t* estimator ) {
	double K[3] = { 1000.0, 275, 0.0250 };
	controller->u = -estimator->stateEstimate[0] * K[0] - estimator->stateEstimate[1] * K[1];
	controller->u += ref_pos * K[0];
}

void sendOutput( output ) {
	ssize_t err;
	char * control_buf = devinf.txBuffer;
	control_buf = 0;

	char c[4];
	c[0] = (char)((output/100))+'0';
	c[1] = (char)((output%100)/10)+'0';
	c[2] = (char)((output%100)%10)+'0';
	c[3] = '\0';

	control_buf = c;
	err = secureWrite(devinf.ttyDev, control_buf, 4);
	puts( c );
	if(err < 0) {
		perror("ERROR DURING TRANSMISSION");
		printf("fd: %d, buf: %s, txLen: 4\r\n", devinf.ttyDev, control_buf);
		running = 0;
		run_program = -1;
	}
}

int readBuffer( unsigned char * buf, int maxRead ) {
	return (int)read(devinf.ttyDev, buf, maxRead );  
}

static void *sampler(void *args) {
	RT_TASK *sampler;
	comedi_t *card;

	// Controller variables
	PILeadController_t controller;
	double time_to_die = 10; //200/30/10; // Time for it to get down

	// For complementary filter
	complementaryFilter_t filter;

	// For state space estimator:
	double initState[3] = { 0 };
	double calibOffset[6] = { -32.6854, 6.9044, 44.0265, 93.6858, -107.6490, 69.1131 };
	double calibScale[6] = { 0.004755001962, 0.004794788965, 0.004778196636, 0.000532632218, 0.000532632218, 0.000532632218 };
	estimator_t* estimator = estimatorInit( initState, calibOffset, calibScale, 9.82, 340.29/1e6, -0.84, 0.02, 1000.0 );

	// Data received from the AD converter
	uint rpot, rpd;
	double pot = 0, pd = 0;
	// Z-acceleration from teensy
	acceleration_t acc;

	// Time variable
	struct timeval t;
	double now;

	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

	if (!(sampler = rt_task_init(nam2num("IB__SM"), 1, 0, 0))) {
		printf("Can't init sampler\n");
		exit(1);
	}

	// Open AD converter
	if (!(card=comedi_open("/dev/comedi2"))) {
		printf("comedi2 not found\n");
		exit(1);
	}

	comedi_data_write(card,1,0,0,AREF_DIFF,2048);

	rt_task_make_periodic_relative_ns(sampler, PERIOD, PERIOD);
	rt_task_wait_period();

	// Initializing controller parameters
	initController( &controller );

	//initialise filter parameters
	initFilter( &filter );

	initAcc( &acc );


	// Main controller loop
	running=100;

	sendOutput( 0 );

	int numBytes = 0;

	//alloc read storage
	unsigned char * recBytes = malloc( sizeof( unsigned char ) * 256 );
	unsigned char * unpackedData = malloc( sizeof( unsigned char ) * 256 );

	//For acceleration, gyro and ultrasound samples
	double acc_gyro[30][6];
	uint16_t ultrasonicDist[30];

	while (running) {

		if (!shutdown) {

			numBytes = readBuffer( recBytes + PACKED_SIZE, 256-PACKED_SIZE );
			numBytes = unpackPackets( PACKED_SIZE, recBytes, numBytes, unpackedData );
			// The following is for use with distance sensor
			/* Get input from AD-converter(dev,subdiv,channel,range,ref,data,delay) */
			comedi_data_read_delayed(card,0,0,0,AREF_DIFF,&rpd,50000);
			double d = (double)(rpd)-32000.0;

			// Convert distance measured to radians for GP2Y0A02YK - close
			pd =  3.1415/180.0*(37691.0*pow(d,-1.338)-0.2427)/0.0183 - 0.0538;

			comedi_data_read_delayed(card,0,1,0,AREF_DIFF,&rpot,50000);

			pot = (((double)(rpot))-32000.0-NULDEG)*RADPERQUANT;

			// decode data coming from tiva launchpad
			int i, j;
			for ( i = 0; i < numBytes/UNPACKED_SIZE; i++ ) {
				for ( j = 0; j < 12; j+=2 ) {
					acc_gyro[i][j/2] = (int16_t)( ( unpackedData[ i*UNPACKED_SIZE + j  ] << 8 ) | ( unpackedData[ i*UNPACKED_SIZE + j + 1 ] ) );
				}
				ultrasonicDist[i] = (uint16_t)( ( unpackedData[ i*UNPACKED_SIZE + 12  ] << 8 ) | ( unpackedData[ i*UNPACKED_SIZE + 13 ] ) );
			}

			//different updates for different control modes
			#ifdef POTMETER
			//Perform predict step
			estimatorPredict( estimator, acc_gyro, numBytes/UNPACKED_SIZE );

			//Perform filter update with desired frequency
			if ( lines%(200/20) == 0)
				estimatorUpdate( estimator, pot );
			#endif

			#ifdef IRSENSOR
			//Perform predict step
			estimatorPredict( estimator, acc_gyro, numBytes/UNPACKED_SIZE );

			//Perform filter update with desired frequency
			if ( lines%(200/20) == 0)
				estimatorUpdate( estimator, pd );
			#endif

			#ifdef ULTRASOUND
			estimatorPredictUpdate( estimator, acc_gyro, ultrasonicDist, numBytes/UNPACKED_SIZE );
			#endif

			// Only run the controller when filter has had a chance to converge
			if ( lines > 800 ) {
				updateController( &controller, estimator );

				// Add the offset that ensures hover-torque
				output = controller.u + controller.offset;
			} else {
				output = 2;
			}

			// limit output so motor will never stop
			if (output < 2) { output = 2; }
			if (output > 255) { output = 255; }
		}
		else {
			output = controller.offset-10;
			time_to_die--;
			if (time_to_die==0) running=0;
		}

		//log some data
		gettimeofday(&t, NULL);
		now = t.tv_sec + t.tv_usec * 1e-6;
		//fprintf(ft,"%ld.%06ld, %f, %f, %f, %f, %f, %f, %d\r\n",t.tv_sec,t.tv_usec,pot,pd,acc.angAccZ,filter.pout,filter.aout,controller.u, output);

		fprintf( ft, "%ld.%06ld, %f, %d, %f, %f, %f, %f", t.tv_sec, t.tv_usec, pot, output, estimator->stateEstimate[0], estimator->stateEstimate[1], estimator->stateEstimate[2], pd );
		int i = 0;
		for ( i = 0; i < numBytes; i+=2 ) {
//			fprintf( ft, ", %d", (int16_t)( ( unpackedData[ i ] << 8 ) | ( unpackedData[ i + 1 ] ) ) );
		}
		fprintf( ft, "\n" );
		lines++;

		sendOutput( output );
		rt_task_wait_period();
	}
	/* Resetting motor */
	comedi_data_write(card,1,0,0,AREF_DIFF,2048);

	estimatorFree( estimator );

	//rt_task_delete(sampler);
	printf("Sampler done\n");
	return 0;
}


int OpenAndSetMode(void) {
	int error;
	devinf.ttyDev = open(devinf.devName,O_RDWR);
	if (devinf.ttyDev != -1) {
		error = set_serial(devinf.ttyDev,devinf.baudrate);
		if (error!=0) {
			printf("Error, serial port could not open (%s)\n",devinf.devName);
			exit(1);
		} else {
			printf("Port open: %s BAUD: %d\n",devinf.devName, devinf.baudrate);
			return 1;
		}
	} else {
		perror("Open device error");
		exit(1);
	}
}

int setupSerial(void) {
	//strncpy(devinf.devName, "/dev/ttyUSB4", MxDL);
	strncpy(devinf.devName, "/dev/ttyACM0", MxDL);
	//devinf.baudrate = 57600;
	devinf.baudrate = 115200;
	//devinf.baudrate = 230400;
	//devinf.baudrate = 9600;
	return 0;
}

int initSerial(void) {
	//Open serial port
	int result;
	result=OpenAndSetMode();
	return result;
}

int main() {
	ft = fopen(filename, "wb");
	if ( ft == NULL ) {
		printf( "failed to open analysis file" );
		return 1;
	}
	printf("Analysis file opened\r\n");
	//     // Setup serial communication
	setupSerial();
	printf("Looking for serial connection\r\n");
	initSerial();
	printf("Serial connection initialized\r\n");

	// Setup real time sampling
	RT_TASK *rttask;
	struct sched_param mysched;
	pthread_t rtthread;
	rt_allow_nonroot_hrt();
	if (!(rttask = rt_task_init(nam2num("IB__MN"), 1, 0, 0))) {
		printf("Can't init master task\n");
		exit(1);
	}
	printf("lxrt task connected\n");

	if (mlockall(MCL_CURRENT | MCL_FUTURE)) {
		perror("mlockall");
		exit(-1);
	}
	printf("locked\n");

	mysched.sched_priority = sched_get_priority_max(SCHED_FIFO) - 1;
	if (sched_setscheduler(0, SCHED_FIFO, &mysched) == -1) {
		puts("Error setting the scheduler");
		perror("errno");
		exit(1);
	}
	printf("soft-rt\n");

	running = 1;
	int error = pthread_create(&rtthread, NULL, sampler, NULL);
	if(error)
	{
		perror("Something went wrong creating 'sampler'");
		return -1;
	}
	printf("Sampler thread created\r\n");


	run_program = 10;

	double input;
	char mode;

	while (run_program >-1) {
		//printf("enter reference \n");
		mode = ' ';

		scanf("%c",&mode);
		if (mode == 'r') { // Change angle ref in degrees
			scanf("%lf",&input);
			ref_pos = input/100;
			if (input > 255) { input = 255; }
		}
		else if (mode == 'x') { // Stop program
			run_program = -1;
		} else {
			// Do nothing
		}



	}
	shutdown = 1;
	//running = 0;
	printf("\r\nExiting\r\n");


	pthread_join(rtthread, NULL);
	printf("Thread joined\r\n");
	rt_task_delete(rttask);
	printf("Task deleted\r\n");

	fclose(ft);
	printf("Analysis file is closed\r\n");
	printf("%d lines of data recorded\r\n",lines);

	return 0;

}

