/* Copyright (c) 2004,2005,2007 Joerg Wunsch
00002    Copyright (c) 2005, Carlos Lamas
00003    All rights reserved.
00004 
00005    Redistribution and use in source and binary forms, with or without
00006    modification, are permitted provided that the following conditions are met:
00007 
00008    * Redistributions of source code must retain the above copyright
00009      notice, this list of conditions and the following disclaimer.
00010 
00011    * Redistributions in binary form must reproduce the above copyright
00012      notice, this list of conditions and the following disclaimer in
00013      the documentation and/or other materials provided with the
00014      distribution.
00015 
00016    * Neither the name of the copyright holders nor the names of
00017      contributors may be used to endorse or promote products derived
00018      from this software without specific prior written permission.
00019 
00020   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
00021   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
00022   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
00023   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
00024   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
00025   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
00026   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
00027   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
00028   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
00029   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
00030   POSSIBILITY OF SUCH DAMAGE. */
 
 /* $Id: inttypes_8h_source.html,v 1.1.1.4 2012/01/03 16:04:22 joerg_wunsch Exp $ */
 
#ifndef __INTTYPES_H_
#define __INTTYPES_H_

#include <stdint.h>

/** \file */
/** \defgroup avr_inttypes <inttypes.h>: Integer Type conversions
00041     \code #include <inttypes.h> \endcode
00042 
00043     This header file includes the exact-width integer definitions from
00044     <tt><stdint.h></tt>, and extends them with additional facilities
00045     provided by the implementation.
00046 
00047     Currently, the extensions include two additional integer types
00048     that could hold a "far" pointer (i.e. a code pointer that can
00049     address more than 64 KB), as well as standard names for all printf
00050     and scanf formatting options that are supported by the \ref avr_stdio.
00051     As the library does not support the full range of conversion
00052     specifiers from ISO 9899:1999, only those conversions that are
00053     actually implemented will be listed here.
00054 
00055     The idea behind these conversion macros is that, for each of the
00056     types defined by <stdint.h>, a macro will be supplied that portably
00057     allows formatting an object of that type in printf() or scanf()
00058     operations.  Example:
00059 
00060     \code
00061     #include <inttypes.h>
00062 
00063     uint8_t smallval;
00064     int32_t longval;
00065     ...
00066     printf("The hexadecimal value of smallval is %" PRIx8
00067            ", the decimal value of longval is %" PRId32 ".\n",
00068            smallval, longval);
00069     \endcode
00070 */

/** \name Far pointers for memory access >64K */

/*@{*/
/** \ingroup avr_inttypes
   signed integer type that can hold a pointer > 64 KB */
typedef int32_t int_farptr_t;

/** \ingroup avr_inttypes
    unsigned integer type that can hold a pointer > 64 KB */
typedef uint32_t uint_farptr_t;
/*@}*/
#if !defined(__cplusplus) || defined(__STDC_LIMIT_MACROS)

/** \name macros for printf and scanf format specifiers

    For C++, these are only included if __STDC_LIMIT_MACROS
    is defined before including <inttypes.h>.
 */

/*@{*/
/** \ingroup avr_inttypes
    decimal printf format for int8_t */
#define         PRId8                   "d"
/** \ingroup avr_inttypes
   decimal printf format for int_least8_t */
#define         PRIdLEAST8              "d"
/** \ingroup avr_inttypes
00101     decimal printf format for int_fast8_t */
#define         PRIdFAST8               "d"

/** \ingroup avr_inttypes
00105     integer printf format for int8_t */
#define         PRIi8                   "i"
/** \ingroup avr_inttypes
00108     integer printf format for int_least8_t */
#define         PRIiLEAST8              "i"
/** \ingroup avr_inttypes
   integer printf format for int_fast8_t */
#define         PRIiFAST8               "i"

/** \ingroup avr_inttypes
00116     decimal printf format for int16_t */
#define         PRId16                  "d"
/** \ingroup avr_inttypes
00119     decimal printf format for int_least16_t */
#define         PRIdLEAST16             "d"
/** \ingroup avr_inttypes
00122     decimal printf format for int_fast16_t */
#define         PRIdFAST16              "d"

/** \ingroup avr_inttypes
00126     integer printf format for int16_t */
#define         PRIi16                  "i"
/** \ingroup avr_inttypes
00129     integer printf format for int_least16_t */
#define         PRIiLEAST16             "i"
/** \ingroup avr_inttypes
00132     integer printf format for int_fast16_t */
#define         PRIiFAST16              "i"


/** \ingroup avr_inttypes
    decimal printf format for int32_t */
#define         PRId32                  "ld"
/** \ingroup avr_inttypes
    decimal printf format for int_least32_t */
#define         PRIdLEAST32             "ld"
/** \ingroup avr_inttypes
    decimal printf format for int_fast32_t */
#define         PRIdFAST32              "ld"

/** \ingroup avr_inttypes
    integer printf format for int32_t */
#define         PRIi32                  "li"
/** \ingroup avr_inttypes
    integer printf format for int_least32_t */
#define         PRIiLEAST32             "li"
/** \ingroup avr_inttypes
    integer printf format for int_fast32_t */
#define         PRIiFAST32              "li"


#ifdef __avr_libc_does_not_implement_long_long_in_printf_or_scanf

#define         PRId64                  "lld"
#define         PRIdLEAST64             "lld"
#define         PRIdFAST64              "lld"

#define         PRIi64                  "lli"
#define         PRIiLEAST64             "lli"
#define         PRIiFAST64              "lli"


#define         PRIdMAX                 "lld"
#define         PRIiMAX                 "lli"
 
#endif

/** \ingroup avr_inttypes
00174     decimal printf format for intptr_t */
#define         PRIdPTR                 PRId16
/** \ingroup avr_inttypes
00177     integer printf format for intptr_t */
#define         PRIiPTR                 PRIi16

/** \ingroup avr_inttypes
00181     octal printf format for uint8_t */
#define         PRIo8                   "o"
/** \ingroup avr_inttypes
00184     octal printf format for uint_least8_t */
#define         PRIoLEAST8              "o"
/** \ingroup avr_inttypes
00187     octal printf format for uint_fast8_t */
#define         PRIoFAST8               "o"

/** \ingroup avr_inttypes
00191     decimal printf format for uint8_t */
#define         PRIu8                   "u"
/** \ingroup avr_inttypes
00194     decimal printf format for uint_least8_t */
#define         PRIuLEAST8              "u"
/** \ingroup avr_inttypes
00197     decimal printf format for uint_fast8_t */
#define         PRIuFAST8               "u"

/** \ingroup avr_inttypes
00201     hexadecimal printf format for uint8_t */
#define         PRIx8                   "x"
/** \ingroup avr_inttypes
00204     hexadecimal printf format for uint_least8_t */
#define         PRIxLEAST8              "x"
/** \ingroup avr_inttypes
00207     hexadecimal printf format for uint_fast8_t */
#define         PRIxFAST8               "x"

/** \ingroup avr_inttypes
00211     uppercase hexadecimal printf format for uint8_t */
#define         PRIX8                   "X"
/** \ingroup avr_inttypes
00214     uppercase hexadecimal printf format for uint_least8_t */
#define         PRIXLEAST8              "X"
/** \ingroup avr_inttypes
00217     uppercase hexadecimal printf format for uint_fast8_t */
#define         PRIXFAST8               "X"


/** \ingroup avr_inttypes
00222     octal printf format for uint16_t */
#define         PRIo16                  "o"
/** \ingroup avr_inttypes
00225     octal printf format for uint_least16_t */
#define         PRIoLEAST16             "o"
/** \ingroup avr_inttypes
00228     octal printf format for uint_fast16_t */
#define         PRIoFAST16              "o"

/** \ingroup avr_inttypes
00232     decimal printf format for uint16_t */
#define         PRIu16                  "u"
/** \ingroup avr_inttypes
00235     decimal printf format for uint_least16_t */
#define         PRIuLEAST16             "u"
/** \ingroup avr_inttypes
00238     decimal printf format for uint_fast16_t */
#define         PRIuFAST16              "u"

/** \ingroup avr_inttypes
00242     hexadecimal printf format for uint16_t */
#define         PRIx16                  "x"
/** \ingroup avr_inttypes
00245     hexadecimal printf format for uint_least16_t */
#define         PRIxLEAST16             "x"
/** \ingroup avr_inttypes
00248     hexadecimal printf format for uint_fast16_t */
#define         PRIxFAST16              "x"

/** \ingroup avr_inttypes
00252     uppercase hexadecimal printf format for uint16_t */
#define         PRIX16                  "X"
/** \ingroup avr_inttypes
00255     uppercase hexadecimal printf format for uint_least16_t */
#define         PRIXLEAST16             "X"
/** \ingroup avr_inttypes
00258     uppercase hexadecimal printf format for uint_fast16_t */
#define         PRIXFAST16              "X"


/** \ingroup avr_inttypes
00263     octal printf format for uint32_t */
#define         PRIo32                  "lo"
/** \ingroup avr_inttypes
00266     octal printf format for uint_least32_t */
#define         PRIoLEAST32             "lo"
 /** \ingroup avr_inttypes
00269     octal printf format for uint_fast32_t */
#define         PRIoFAST32              "lo"

/** \ingroup avr_inttypes
00273     decimal printf format for uint32_t */
#define         PRIu32                  "lu"
/** \ingroup avr_inttypes
00276     decimal printf format for uint_least32_t */
#define         PRIuLEAST32             "lu"
/** \ingroup avr_inttypes
00279     decimal printf format for uint_fast32_t */
#define         PRIuFAST32              "lu"

/** \ingroup avr_inttypes
00283     hexadecimal printf format for uint32_t */
#define         PRIx32                  "lx"
/** \ingroup avr_inttypes
00286     hexadecimal printf format for uint_least32_t */
#define         PRIxLEAST32             "lx"
/** \ingroup avr_inttypes
00289     hexadecimal printf format for uint_fast32_t */
#define         PRIxFAST32              "lx"

/** \ingroup avr_inttypes
00293     uppercase hexadecimal printf format for uint32_t */
#define         PRIX32                  "lX"
/** \ingroup avr_inttypes
00296     uppercase hexadecimal printf format for uint_least32_t */
#define         PRIXLEAST32             "lX"
/** \ingroup avr_inttypes
00299     uppercase hexadecimal printf format for uint_fast32_t */
#define         PRIXFAST32              "lX"


#ifdef __avr_libc_does_not_implement_long_long_in_printf_or_scanf

#define         PRIo64                  "llo"
#define         PRIoLEAST64             "llo"
#define         PRIoFAST64              "llo"

#define         PRIu64                  "llu"
#define         PRIuLEAST64             "llu"
#define         PRIuFAST64              "llu"

#define         PRIx64                  "llx"
#define         PRIxLEAST64             "llx"
#define         PRIxFAST64              "llx"

#define         PRIX64                  "llX"
#define         PRIXLEAST64             "llX"
#define         PRIXFAST64              "llX"

#define         PRIoMAX                 "llo"
#define         PRIuMAX                 "llu"
#define         PRIxMAX                 "llx"
#define         PRIXMAX                 "llX"

#endif

/** \ingroup avr_inttypes
00329     octal printf format for uintptr_t */
#define         PRIoPTR                 PRIo16
/** \ingroup avr_inttypes
00332     decimal printf format for uintptr_t */
#define         PRIuPTR                 PRIu16
/** \ingroup avr_inttypes
00335     hexadecimal printf format for uintptr_t */
#define         PRIxPTR                 PRIx16
/** \ingroup avr_inttypes
00338     uppercase hexadecimal printf format for uintptr_t */
#define         PRIXPTR                 PRIX16


#ifdef __avr_libc_does_not_implement_hh_in_scanf

#define         SCNd8                   "hhd"
#define         SCNdLEAST8              "hhd"
#define         SCNdFAST8               "hhd"

#define         SCNi8                   "hhi"
#define         SCNiLEAST8              "hhi"
#define         SCNiFAST8               "hhi"

#endif


/** \ingroup avr_inttypes
00356     decimal scanf format for int16_t */
#define         SCNd16                  "d"
/** \ingroup avr_inttypes
00359     decimal scanf format for int_least16_t */
#define         SCNdLEAST16             "d"
/** \ingroup avr_inttypes
00362     decimal scanf format for int_fast16_t */
#define         SCNdFAST16              "d"

/** \ingroup avr_inttypes
00366     generic-integer scanf format for int16_t */
#define         SCNi16                  "i"
/** \ingroup avr_inttypes
00369     generic-integer scanf format for int_least16_t */
#define         SCNiLEAST16             "i"
/** \ingroup avr_inttypes
00372     generic-integer scanf format for int_fast16_t */
#define         SCNiFAST16              "i"


/** \ingroup avr_inttypes
00377     decimal scanf format for int32_t */
#define         SCNd32                  "ld"
/** \ingroup avr_inttypes
00380     decimal scanf format for int_least32_t */
#define         SCNdLEAST32             "ld"
/** \ingroup avr_inttypes
00383     decimal scanf format for int_fast32_t */
#define         SCNdFAST32              "ld"

/** \ingroup avr_inttypes
00387     generic-integer scanf format for int32_t */
#define         SCNi32                  "li"
/** \ingroup avr_inttypes
00390     generic-integer scanf format for int_least32_t */
#define         SCNiLEAST32             "li"
/** \ingroup avr_inttypes
00393     generic-integer scanf format for int_fast32_t */
#define         SCNiFAST32              "li"


#ifdef __avr_libc_does_not_implement_long_long_in_printf_or_scanf

#define         SCNd64                  "lld"
#define         SCNdLEAST64             "lld"
#define         SCNdFAST64              "lld"

#define         SCNi64                  "lli"
#define         SCNiLEAST64             "lli"
#define         SCNiFAST64              "lli"

#define         SCNdMAX                 "lld"
#define         SCNiMAX                 "lli"

#endif

/** \ingroup avr_inttypes
00413     decimal scanf format for intptr_t */
#define         SCNdPTR                 SCNd16
/** \ingroup avr_inttypes
00416     generic-integer scanf format for intptr_t */
#define         SCNiPTR                 SCNi16

#ifdef __avr_libc_does_not_implement_hh_in_scanf

#define         SCNo8                   "hho"
#define         SCNoLEAST8              "hho"
#define         SCNoFAST8               "hho"

#define         SCNu8                   "hhu"
#define         SCNuLEAST8              "hhu"
#define         SCNuFAST8               "hhu"

#define         SCNx8                   "hhx"
#define         SCNxLEAST8              "hhx"
#define         SCNxFAST8               "hhx"

#endif

/** \ingroup avr_inttypes
00436     octal scanf format for uint16_t */
#define         SCNo16                  "o"
/** \ingroup avr_inttypes
00439     octal scanf format for uint_least16_t */
#define         SCNoLEAST16             "o"
/** \ingroup avr_inttypes
00442     octal scanf format for uint_fast16_t */
#define         SCNoFAST16              "o"

/** \ingroup avr_inttypes
00446     decimal scanf format for uint16_t */
#define         SCNu16                  "u"
/** \ingroup avr_inttypes
00449     decimal scanf format for uint_least16_t */
#define         SCNuLEAST16             "u"
/** \ingroup avr_inttypes
00452     decimal scanf format for uint_fast16_t */
#define         SCNuFAST16              "u"

/** \ingroup avr_inttypes
00456     hexadecimal scanf format for uint16_t */
#define         SCNx16                  "x"
/** \ingroup avr_inttypes
00459     hexadecimal scanf format for uint_least16_t */
#define         SCNxLEAST16             "x"
/** \ingroup avr_inttypes
00462     hexadecimal scanf format for uint_fast16_t */
#define         SCNxFAST16              "x"


/** \ingroup avr_inttypes
00467     octal scanf format for uint32_t */
#define         SCNo32                  "lo"
/** \ingroup avr_inttypes
00470     octal scanf format for uint_least32_t */
#define         SCNoLEAST32             "lo"
/** \ingroup avr_inttypes
00473     octal scanf format for uint_fast32_t */
#define         SCNoFAST32              "lo"

/** \ingroup avr_inttypes
00477     decimal scanf format for uint32_t */
#define         SCNu32                  "lu"
/** \ingroup avr_inttypes
00480     decimal scanf format for uint_least32_t */
#define         SCNuLEAST32             "lu"
/** \ingroup avr_inttypes
00483     decimal scanf format for uint_fast32_t */
#define         SCNuFAST32              "lu"
 
/** \ingroup avr_inttypes
    hexadecimal scanf format for uint32_t */
#define         SCNx32                  "lx"
/** \ingroup avr_inttypes
    hexadecimal scanf format for uint_least32_t */
#define         SCNxLEAST32             "lx"
/** \ingroup avr_inttypes
    hexadecimal scanf format for uint_fast32_t */
#define         SCNxFAST32              "lx"


#ifdef __avr_libc_does_not_implement_long_long_in_printf_or_scanf

#define         SCNo64                  "llo"
#define         SCNoLEAST64             "llo"
#define         SCNoFAST64              "llo"

#define         SCNu64                  "llu"
#define         SCNuLEAST64             "llu"
#define         SCNuFAST64              "llu"

#define         SCNx64                  "llx"
#define         SCNxLEAST64             "llx"
#define         SCNxFAST64              "llx"

#define         SCNoMAX                 "llo"
#define         SCNuMAX                 "llu"
#define         SCNxMAX                 "llx"

#endif

/** \ingroup avr_inttypes
    octal scanf format for uintptr_t */
#define         SCNoPTR                 SCNo16
/** \ingroup avr_inttypes
    decimal scanf format for uintptr_t */
#define         SCNuPTR                 SCNu16
/** \ingroup avr_inttypes
    hexadecimal scanf format for uintptr_t */
#define         SCNxPTR                 SCNx16

/*@}*/


#endif  /* !defined(__cplusplus) || defined(__STDC_LIMIT_MACROS) */


#endif /* __INTTYPES_H_ */