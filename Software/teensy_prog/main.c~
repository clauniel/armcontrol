#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdint.h>
#include <util/delay.h>
#include "usb_serial.h"
#include <i2cmaster.h>

#define LED_CONFIG	(DDRD |= (1<<6))
#define LED_ON		(PORTD |= (1<<6))
#define LED_OFF		(PORTD &= ~(1<<6))
#define CPU_PRESCALE(n) (CLKPR = 0x80, CLKPR = (n))

#define TWI_BLCTRL_BASEADDR 0x52
/* MPU6050 */
#define MPU6050_ACC_ADDR 0b11010000
#define ACCEL_XOUT_H 0x3B
#define ACCEL_XOUT_L 0x4C
#define ACCEL_YOUT_H 0x3D
#define ACCEL_YOUT_L 0x3E
#define ACCEL_ZOUT_H 0x3F
#define ACCEL_ZOUT_L 0x40


#define GYRO_YOUT_H 0x45
#define GYRO_YOUT_L 0x46
#define ACCEL_CONFIG 0x1C
#define GYRO_CONFIG 0x1B
#define POWER_MGMGT_1 0x6B
/********/

/* MPU9150 */
//#define MPU9150_TWI_ADDR 0b11010000 /* Same as 6050 */
//Same ACCEL_ZOUT_H as 6050
//Same ACCEL_ZOUT_L as 6050
//Uses same ACCEL_CONFIG as 6050
//Same POWER_MGMGT_1 as 6050
//In order to get acc-z data from 9150 the same functions can be used as the 6050
/********/

//uint16_t accZ = 0;
uint16_t gyroY = 0;
uint8_t accZH = 0;
uint8_t accZL = 0;
uint8_t accYH = 0;
uint8_t accYL = 0;
uint8_t accXH = 0;
uint8_t accXL = 0;
uint8_t gyroyH = 0;
uint8_t gyroyL = 0;


void parse_and_execute_command(const char *buf, uint8_t num);
void send_str(const char *s);
void send_str1(char *s);
uint8_t recv_str(char *buf, uint8_t size);

void MPU6050_init() {
  //Set clock source
  uint8_t ret = i2c_start(MPU6050_ACC_ADDR + I2C_WRITE);
  if (ret) {
    // Release bus
    i2c_stop();
    // Print an error
    LED_ON;
    _delay_ms(2);
    LED_OFF;
  }
  else {
    i2c_write(POWER_MGMGT_1);
    i2c_write(0b00001010);//Set clock -> PLL with Y axis gyroscope reference and temp. sens. off, disable sleep
    
    i2c_stop();
  }
  //set accelerometer scale range
  //uint8_t ret = i2c_start(TWI_ACC_ADDR + I2C_WRITE);
  ret = i2c_start(MPU6050_ACC_ADDR + I2C_WRITE);
  if (ret) {
    // Release bus
    i2c_stop();
    // Print an error
    LED_ON;
    _delay_ms(2);
    LED_OFF;
  }
  else {
    i2c_write(ACCEL_CONFIG);
    i2c_write(0);
    /* ACCEL_CONFIG register:
     * XA_ST YA_ST ZA_ST AFS_SEL[1:0] - - -
     * AFS_SEL  FULL SCALE RANGE
     *  0	+-2g 0
     *  1	+-4g (1<<4)
     *  2	+-8g 1<<5
     *  3	+-16g 0b00011000
     * */
    i2c_stop();
  }
  /*ret = i2c_start(MPU6050_ACC_ADDR + I2C_WRITE);
  if (ret) {
    // Release bus
    i2c_stop();
    // Print an error
    LED_ON;
    _delay_ms(2);
    LED_OFF;
  }
  else {
    i2c_write(GYRO_CONFIG);
    i2c_write(0);
    i2c_stop();
  }*/  
}

void MPU6050_get_accz() {
  uint8_t ret = i2c_start(MPU6050_ACC_ADDR + I2C_WRITE);
  if (ret) {
    // Release bus
    i2c_stop();
    // Print an error
    LED_ON;
    _delay_ms(2);
    LED_OFF;
  }
  else {
    i2c_write(ACCEL_ZOUT_H);
    i2c_start(MPU6050_ACC_ADDR + I2C_READ);
    //accZH = i2c_readNak();
    accZH = i2c_readNak();

    i2c_start(MPU6050_ACC_ADDR + I2C_WRITE);
    i2c_write(ACCEL_ZOUT_L);
    i2c_start(MPU6050_ACC_ADDR + I2C_READ);
    //accZL = i2c_readNak();
    accZL = i2c_readNak();

    
    i2c_stop();
  }
  /*char str[6];
  int ac = accz+2000;
  double dc = (double)ac;
  sprintf(str, "%d\r\n", ac);
  send_str1(str);*/
  //return (accZH<<8)+accZL;
}

void MPU6050_get_accdata() {
  uint8_t ret = i2c_start(MPU6050_ACC_ADDR + I2C_WRITE);
  if (ret) {
    // Release bus
    i2c_stop();
    // Print an error
    LED_ON;
    _delay_ms(2);
    LED_OFF;
  }
  else {
    i2c_write(ACCEL_ZOUT_H);
    i2c_start(MPU6050_ACC_ADDR + I2C_READ);
    accZH = i2c_readNak();

    i2c_start(MPU6050_ACC_ADDR + I2C_WRITE);
    i2c_write(ACCEL_ZOUT_L);
    i2c_start(MPU6050_ACC_ADDR + I2C_READ);
    accZL = i2c_readNak();

    
    i2c_stop();
  }
  ret = i2c_start(MPU6050_ACC_ADDR + I2C_WRITE);
  if (ret) {
    // Release bus
    i2c_stop();
    // Print an error
    LED_ON;
    _delay_ms(2);
    LED_OFF;
  }
  else {
    i2c_write(ACCEL_YOUT_H);
    i2c_start(MPU6050_ACC_ADDR + I2C_READ);
    accYH = i2c_readNak();

    i2c_start(MPU6050_ACC_ADDR + I2C_WRITE);
    i2c_write(ACCEL_YOUT_L);
    i2c_start(MPU6050_ACC_ADDR + I2C_READ);
    accYL = i2c_readNak();

    
    i2c_stop();
  }
  ret = i2c_start(MPU6050_ACC_ADDR + I2C_WRITE);
  if (ret) {
    // Release bus
    i2c_stop();
    // Print an error
    LED_ON;
    _delay_ms(2);
    LED_OFF;
  }
  else {
    i2c_write(ACCEL_XOUT_H);
    i2c_start(MPU6050_ACC_ADDR + I2C_READ);
    accXH = i2c_readNak();

    i2c_start(MPU6050_ACC_ADDR + I2C_WRITE);
    i2c_write(ACCEL_XOUT_L);
    i2c_start(MPU6050_ACC_ADDR + I2C_READ);
    accXL = i2c_readNak();

    
    i2c_stop();
  }
  /*char str[6];
  int ac = accz+2000;
  double dc = (double)ac;
  sprintf(str, "%d\r\n", ac);
  send_str1(str);*/
}

int MPU6050_get_gyroy() {
  uint8_t ret = i2c_start(MPU6050_ACC_ADDR + I2C_WRITE);
  if (ret) {
    // Release bus
    i2c_stop();
    // Print an error
    LED_ON;
    _delay_ms(2);
    LED_OFF;
  }
  else {
    i2c_write(GYRO_YOUT_H);
    i2c_start(MPU6050_ACC_ADDR + I2C_READ);
    gyroyH = i2c_readNak();

    i2c_start(MPU6050_ACC_ADDR + I2C_WRITE);
    i2c_write(GYRO_YOUT_L);
    i2c_start(MPU6050_ACC_ADDR + I2C_READ);
    gyroyL |= i2c_readNak();

    
    i2c_stop();
  }
  /*char str[6];
  int ac = accz+2000;
  double dc = (double)ac;
  sprintf(str, "%d\r\n", ac);
  send_str1(str);*/
  return (gyroyH<<8)+gyroyL;
}

void i2c_send_speed(unsigned char address, uint8_t signal) {
  uint8_t ret = i2c_start(address + I2C_WRITE);
  if (ret) {
    // Release bus
    i2c_stop();
    // Print an error
    LED_ON;
    _delay_ms(50);
    LED_OFF;
  }
  else {
    i2c_write(signal);
    i2c_stop();
  }
  //_delay_ms(1);
}



int localEcho = 1;
uint8_t speed = 1;

// Basic command interpreter for controlling port pins
int main(void)
{
  char buf[32];
  uint8_t n;
  const char* help = PSTR("\n"
    "Simplified, tethered drone USB interface\r\n"
    "xxx	Signal to i2c, (xxx, 0-255)\r\n");
  
  
  // 16 MHz CPU and configure LED
  CPU_PRESCALE(0);
  LED_CONFIG;
  LED_OFF;

  i2c_init();

  MPU6050_init();

  
  usb_init();
  while (!usb_configured())
    /* wait */ ;
  // and a bit more
  _delay_ms(1000);
  DDRD|=(1 << PD0);
  OCR0B=127;
  DDRB|=(1 << PB7);
  OCR0A=127;
  
 speed = 1; // If speed=0, motor will not run, need to be at least 1 for motor to rotate
  
  // This is just to indicate that the teensy has been turned on
  //i2c_send_speed(TWI_BLCTRL_BASEADDR, speed);
  
  while(1)
  {
    // wait for the user to run their terminal emulator program
    // which sets DTR to indicate it is ready to receive.
    while (!(usb_serial_get_control() & USB_SERIAL_DTR))
    {
     //i2c_send_speed(TWI_BLCTRL_BASEADDR, speed); 
    }/* wait */ ;

    // discard anything that was received prior.  Sometimes the
    // operating system or other software will send a modem
    // "AT command", which can still be buffered.
    usb_serial_flush_input();

    // print a nice welcome message
    //send_str(help);
    
    while(1) {
      buf[0] = '0';
      buf[1] = '0';
      buf[2] = '0';
      n = recv_str(buf, sizeof(buf));
      //send_str1(buf);
      if (n = 255) {
	//break;
      }
      if (buf[0] == '3') {
	//send_str(help);
      }
      else {
	//send_str1("\r\n");
	parse_and_execute_command(buf,n);
      }

    }

  }
  
  return 0;
}

void send_str(const char *s)
{
//   int n;
//   const char * p1 = s;
//   while (*p1)
//     p1++;
//   n = p1 - s;
//   usb_serial_write(s, n);
//   usb_serial_flush_output();
  char c;
  while (1)
  {
    c = pgm_read_byte(s++);
    if (!c) break;
    usb_serial_putchar(c);
  }
}

void send_str1(char *s)
{
  char c;
  while (1)
  {
    c = *(s++);
    if (!c) break;
    usb_serial_putchar(c);
  }
}

uint8_t recv_str(char *buf, uint8_t size)
{
  int16_t r;
  uint8_t count=0;

  while (count < size)
  {
    r = usb_serial_getchar();
    if (r != -1)
    {
      if (r == '\r' || r == '\n' || r == '\0') {
        return count;
      }
      if (r >= ' ' && r <= '~')
      { // save character in buffer
        *buf++ = r;
        /*if (localEcho)
          // do we want an echo of what we send?
          usb_serial_putchar(r);*/
        count++;
      }
    }
    else
    {
      if (!usb_configured() ||
          !(usb_serial_get_control() & USB_SERIAL_DTR))
      { // user no longer connected
        return 255;
      }
      // just a normal timeout, keep waiting
      MPU6050_get_accz();
      //MPU6050_get_accdata();
      //gyroY = MPU6050_get_gyroy();
      //sprintf(str, "%c%c\0",(char)(accZH),(char)(accZL));
      //sprintf(str, "%c%c\r\n",(char)(accZ[0]),(char)(accZ[1]));
      char str[7] = {0};
      sprintf(str, "%u\r\n",(accZH<<8)+accZL+32000);
      //sprintf(str, "%05u %05u %05u\r\n",(accXH<<8)+accXL+32000,(accYH<<8)+accYL+32000,(accZH<<8)+accZL+32000);
      
      //accZH = 0;
      //accZL = 0;
      //
      //sprintf(str, "%c%c%c%c%c%c%c%c\r\n", (char)(accX[0]),(char)(accX[1]),(char)(accY[0]),(char)(accY[1]),(char)(accZ[0]),(char)(accZ[1]),(char)(gyroyH),(char)(gyroyL));
      //sprintf(str, "%d %d\r\n",accZ, gyroY);
      //send_str1(str);
      //sprintf(str, "%u\r\n",(accZH<<8)+accZL+32000);
      send_str1(str);
      i2c_send_speed(TWI_BLCTRL_BASEADDR, speed);
      
    }
  }
  return count;
}

void parse_and_execute_command(const char *buf, uint8_t num)
{
  
  //uint8_t port = 0, pin = 0, val2;
  //uint16_t val;
  //char isOK = 0;
  uint8_t val;

  if (num < 3) 
  {    
    send_str1("unrecognized format, 3 chars min req'd\r\n");
    return;
  }
  else {
      val=buf[0]-'0';
      val*=10;
      val=buf[1]-'0'+ val;
      val*=10;
      val=buf[2]-'0'+val;
      
      if (val > 255) val = 255;
      if (val < 0) val = 0;

      speed = val;
    
  }
}


