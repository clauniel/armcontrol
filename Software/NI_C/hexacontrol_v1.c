#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/io.h>
#include <sched.h>
#define KEEP_STATIC_INLINE
#include <rtai_lxrt.h>
#include <rtai_comedi.h>
#include <rtai_sched.h>
//#include <comedilib.h>

#include <math.h>

#include "util/globalfunc.h"
#include "util/encode.h"
#include "hexacontrol_v1.h"




#define MILLISEC(x) (1000000 * (x)) /* ms -> ns */
#define PERIOD      MILLISEC(30)

/* How often the hexacopter debug subscription is renewed.
   Must be done at least every 4 seconds */
#define DEBUG_REQUEST_PERIOD  3000

#define SOFT_RT

static int running=1;
double ref_deg=0;
double dis_deg=0;


struct {
    /** is receive thread running */
    int running;

    /** thread handle and attributes */
    pthread_t serial_task;
} rxtask;

// txDeBuffer holds the non-encoded (decoded) data to be transmittee to the Hexacopter
#define MK_MAX_TX_DE_BUFFER 170
unsigned char txDeBuffer[MK_MAX_TX_DE_BUFFER];
// rxEnBuffer holds the encoded data recieved from the Hexacopter
#define MK_MAX_RX_EN_BUFFER 170
unsigned char rxEnBuffer[MK_MAX_RX_EN_BUFFER];
// rxDeBuffer holds the decoded data recieved from the Hexacopter
#define MK_MAX_RX_DE_BUFFER 170
unsigned char rxDeBuffer[MK_MAX_RX_DE_BUFFER];

struct str_DebugOut debugOut;
struct str_ExternControl externCtrl;

int thrust;
const double thrustOffset  = 134;
double thrustControl;

///////////////////////////////////////
// Serial related methods
int initSerial(void);
int OpenAndSetMode(void);
int setupSerial(void);
void * Hexacopter(void * not_used);
///////////////////////////////////////

const char* control_file = "control_log_sys_step_log_fast_ramp_.txt";
const char* debug_file = "debug_log_sys_step_log_fast_ramp_.txt";



// Controller constants    
const double Kpos = 160;

const double b0 = 10;
const double b1 = -9.426; 
const double b2 = 0;
const double a1 = -0.4258; 
const double a2 = 0;



int millisecToTicks(int millisec) {
    return millisec/(int)(PERIOD/1000000);
}


static void *sampler(void *args) {
    RT_TASK *sampler;

    int debugRequestTicks = (int)DEBUG_REQUEST_PERIOD/(int)(PERIOD/1000000);

    // Number of loops done in the sampler thread
    int ticks = debugRequestTicks; // Debug request will be send at system start
    
    comedi_t *card;
    
    // Controller variables
    double x,e,u,e1,e2,u1,u2;
    double x_rad,x_deg;
    double ref_rad;
    double dis_rad;
    
    double in,in1,in2,out,out1,out2;
   
    // Data received from the AD converter
    uint data;
    
    // Time variable
    struct timeval t;
    double now;
    
    // File 
    FILE* fid;
    fid = fopen(control_file,"w");
    fprintf(fid,"header\n");
    fprintf(fid,"Kpos: %1.3f. b0: %1.3f. b1: %1.3f. b2: %1.3f. a1: %1.3f. a2: %1.3f. thrustOffset: %1.2f. ts: %d [ns]\n",
	    Kpos,b0,b1,b2,a1,a2,thrustOffset,PERIOD);
    fprintf(fid,"timestamp,ref_angle,meas_angle,dist_angle,gas_offset,gas,controlsig\n");

    
    
    
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

    if (!(sampler = rt_task_init(nam2num("IB__SM"), 1, 0, 0))) {
        printf("Can't init sampler\n");
        exit(1);
    }
    
    
    while (running) {

        // Open AD converter
        if (!(card=comedi_open("/dev/comedi2"))) {
            printf("comedi2 not found\n");
            exit(1);
        }

        ///comedi_data_write(card,1,0,0,AREF_GROUND,2048);

        rt_task_make_periodic_relative_ns(sampler, PERIOD, PERIOD);
        rt_task_wait_period();

        // Initializing controller parameters
        e1 = 0;
        e2 = 0;
        u1 = 0;
        u2 = 0;
	
	in1 = 0;
	in2 = 0;
	out1 = 0;
	out2 = 0;

        // Main controller loop
        running=100;
        while (running) {
	    
	    /* Get input from AD-converter(dev,subdiv,channel,range,ref,data,delay) */
	    comedi_data_read_delayed(card,0,0,0,AREF_GROUND,&data,20000);

	    // Ref converter
	    // pi/180 ~= 0.01745
	    ref_rad = ref_deg*0.01745;
	    dis_rad = dis_deg*0.01745;
	    // #########################

	  
	   /* // #########################
	    // 5 VOLT ON POTENTIOMETER
            // convert to height in m
            // 0 deg ~= 2086
            // 90 deg ~= 2384
            // (pi/2)/(2384-2086) = 0.00527
            x_rad = ((double)data-2086.0)*0.00527;
            // 180/pi ~= 57.2958
            x_deg = x_rad*57.2958;
            x = sin(x_rad);
            //printf("Raw pot data: %d. Deg: %f\n",data,x_deg);

            // Ref converter
            // pi/180 ~= 0.01745
            ref_rad = ref_deg*0.01745;
	    // ######################### */
	    
	    // #########################
	    // 15 VOLT ON POTENTIOMETER
            // convert to height in m
	    // 0 deg ~= 2106
            // 90 deg ~= 2979
            // (pi/2)/(2979-2106) = 0.001799
	    
	    x_rad = ((double)data-2106.0)*0.001799;
            // 180/pi ~= 57.2958
            x_deg = x_rad*57.2958;
            x = sin(x_rad+dis_rad); // With disturbance on measurement
            //printf("Raw pot data: %d. Deg: %f\n",data,x_deg);

            
	        
	    

	    // PD controller (lead in feedback)
	    in = x;
	    out = b0*in+((b1*in1+b2*in2)-(a1*out1+a2*out2));
	    
	    in2 = in1;
	    in1 = in;
	    out2 = out1;
	    out1 = out;
	    
	    e = sin(ref_rad)-out;
	    u = Kpos*e;
	    

	    // send new control signal
	    thrust = (int) u+thrustOffset;
        
	    int n;
	    int m;
	    char * control_buf = devinf.txBuffer;
	    // get control data
	    externCtrl.Nick = limitUnsignedChar(0, -128, 127);
	    externCtrl.Roll = limitSignedChar(0, -128, 127);
	    externCtrl.Gier = limitSignedChar(0, -128, 127);
	    externCtrl.Gas = limitUnsignedChar(thrust, 0, 200);
	    externCtrl.Height = limitInt(0, 0, 4000);
	    externCtrl.Frame = 0;    
	    externCtrl.Config = 1;
	    
	    // pack message
	    m = sizeof(externCtrl);
	    n = packData(control_buf, 'b', FC_ADDRESS, (unsigned char *)&externCtrl, m);
// 	    secureWrite(devinf.ttyDev, control_buf, n);



            if (ticks>debugRequestTicks) {
		
		/* Give copter time to receive external control command before
		   sending debug request. */
		rt_sleep(nano2count(MILLISEC(8))); // 8 millisec based on packages loss test
	      
                // Renew debug subscription
                int n;
                char * buf = devinf.txBuffer;
                unsigned char dataReq = 5; // update rate x 10 ms
                n = packData(buf, 'd', FC_ADDRESS, &dataReq, 1);

                //Send the Debug Request
//                 secureWrite(devinf.ttyDev, buf, n);

                ticks = 0;
//                 printf("Debug request sent\n");
//                 printf("Control signal: %d. Ref: %3.1f [deg]. Actual: %3.1f [deg]. Error: %3.3f [m]\n",thrust,ref_deg,x_deg,e);
// 		printf("Disturbance on measurement: %3.1f [deg]\n",dis_deg);
		
		printf("Data from NI: %d\n",data);
		printf("Angle from potentiometer: %1.2lf\n",x_deg);

            } else {
		

	      
	    }
	    
	    gettimeofday(&t, NULL);
	    now = t.tv_sec + t.tv_usec * 1e-6;	    
	    fprintf(fid,"%f,%2.3f,%2.3f,%2.3f,%2.1f,%d,%3.3f\n",now,ref_deg,x_deg,dis_deg,thrustOffset,thrust,u);
	    
	    
		
            ticks++;

	    

	    
            rt_task_wait_period();
        }
        /* Resetting motor */
        ///comedi_data_write(card,1,0,0,AREF_GROUND,2048);
	
	// Close files
	fclose(fid);

    }


    rt_task_delete(sampler);
    printf("Sampler done\n");
    return 0;
}


int OpenAndSetMode(void) {
    int error;
    devinf.ttyDev = open(devinf.devName,O_RDWR);
    if (devinf.ttyDev != -1) {
        error = set_serial(devinf.ttyDev,devinf.baudrate);
        if (error!=0) {
            printf("Error, serial port could not open (%s)\n",devinf.devName);
            exit(1);
        } else {
            printf("Port open: %s BAUD: %d\n",devinf.devName, devinf.baudrate);
            return 1;
        }
    } else {
        perror("Open device error");
        exit(1);
    }
}

int setupSerial(void) {
    strncpy(devinf.devName, "/dev/ttyUSB4", MxDL);
    devinf.baudrate = 57600;
    return 0;
}

int initSerial(void) {
    //Open serial port
    int result;
    //
    result=OpenAndSetMode();
    //
    if (result == 1) {
        // start thread to handle bus
        pthread_attr_t attr;
        pthread_attr_init(&attr);
        pthread_attr_setinheritsched(&attr, PTHREAD_INHERIT_SCHED);
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
        if (pthread_create(&rxtask.serial_task, &attr, Hexacopter, 0)) {
            perror(": Can't start serial receive thread");
            result = 0;
        }
    }
    if (result == 1) {
        int waitCount = 0;
        while (!rxtask.running) { // wait a bit for thread to start
            usleep(20000); //Don't return before threads are running
            if (waitCount >= 50) {
                result = 0;
                break;
            }
            waitCount++;
        }
    }
    return result;
}

/** The Hexacopter thread reads the input recieved from the Hexacopter
 * First it waits for the carriage return after start byte (#)
 * then it sees if the package has the right command ID
 * If so it decodes the package */
void * Hexacopter(void * not_used) {

    printf("Read Thread started\n");
    rxtask.running = 1;

    unsigned char rx, end;
    
    // Time variable
    struct timeval t;
    double now;
    
    // Log file
    FILE* fid;
    fid = fopen(debug_file,"w");
    fprintf(fid,"header\n");
    fprintf(fid,"Kpos: %1.3f. b0: %1.3f. b1: %1.3f. b2: %1.3f. a1: %1.3f. a2: %1.3f. thrustOffset: %1.2f. ts: %d [ns]\n",
	    Kpos,b0,b1,b2,a1,a2,thrustOffset,PERIOD);
    fprintf(fid,"timestamp,anglenick,angleroll,accnick,accroll,yawgyro,height,accz,"
	         "gas,compass,gyrocompass,"
	         "motor1,motor2,motor3,motor4,motor5,motor6,motor7,motor8,"
	         "servo,hovergas\n");
    
    while (rxtask.running) {

        rxEnBuffer[2] = '\0';
        rx = 0;
        end = 0;

        /*Recieve until carriage return*/
        while (rx!='\r') { // look for data from # to 'carriage return' character
            read(devinf.ttyDev,&rx,1);
            rxEnBuffer[end] = rx;
            if (end > 0 || rx == '#')
                end++;
            if (end >= MK_MAX_RX_EN_BUFFER) { // too long, must be garbage, restart
                end = 0;
                break;
            }
        }

        // Labels of the analog values in the Debug Data Struct received in "D"
        if (rxEnBuffer[2] == 'A') {
            int n;
            n = decode64(rxDeBuffer, sizeof(rxDeBuffer),3, end, rxEnBuffer);

        }
	
	// Extern Control Frame Echo
        if (rxEnBuffer[2] == 'B') {
	    int n;
	    n = decode64(rxDeBuffer,sizeof(rxDeBuffer),3,end,rxEnBuffer);
	       


        }
	
        // Data3D structure
        if (rxEnBuffer[2] == 'C') {

        }

        // External control values
        if (rxEnBuffer[2] == 'G') {

        }

        // Version info
        if (rxEnBuffer[2] == 'V') {

        }

        // Related to compass heading (Nick Roll Attitude...)
        if (rxEnBuffer[2] == 'k') {

        }

        // Debug data. Analog sensor values. See Debug struct
        if (rxEnBuffer[2] == 'D') {
            decode64((unsigned char *)&debugOut, sizeof(debugOut),3, end, rxEnBuffer);

            gettimeofday(&t, NULL);
            now = t.tv_sec + t.tv_usec * 1e-6;
            //printf("Got D at time %f \n",now);
	    
// 	    "timestamp,anglenick,angleroll,accnick,accroll,yawgyro,height,accz,"
// 	    "gas,compass,gyrocompass,"
// 	    "motor1,motor2,motor3,motor4,motor5,motor6,motor7,motor8,"
// 	    "servo,hovergas" 21 TOTAL
	    fprintf(fid,"%f,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",
		    now,			
		    debugOut.AngleNick,
		    debugOut.AngleRoll,
		    debugOut.AccNick,
		    debugOut.AccRoll,
		    debugOut.YawGyro,
		    debugOut.HeightValue,
		    debugOut.AccZ,
		    debugOut.Gas,
		    debugOut.CompassValue,
		    debugOut.GyroCompass,
		    debugOut.Motor1,
		    debugOut.Motor2,
		    debugOut.Motor3,
		    debugOut.Motor4,
		    debugOut.Motor5,
		    debugOut.Motor6,
		    debugOut.Motor7,
		    debugOut.Motor8,
		    debugOut.Servo,
		    debugOut.Hovergas);

        }
    }

    rxtask.running = 0;
    
    fclose(fid);
    
    printf("Read Thread ended\n");
    return NULL;
}

int main() {

//     // Setup serial communication
//     setupSerial();
//     initSerial();

    // Setup real time sampling
    RT_TASK *rttask;
    struct sched_param mysched;
    pthread_t rtthread;
    rt_allow_nonroot_hrt();
    if (!(rttask = rt_task_init(nam2num("IB__MN"), 1, 0, 0))) {
        printf("Can't init master task\n");
        exit(1);
    }
    printf("lxrt task connected\n");
    
    if (mlockall(MCL_CURRENT | MCL_FUTURE)) {
        perror("mlockall");
        exit(-1);
    }
    printf("locked\n");

    mysched.sched_priority = sched_get_priority_max(SCHED_FIFO) - 1;
    if (sched_setscheduler(0, SCHED_FIFO, &mysched) == -1) {
        puts("Error setting the scheduler");
        perror("errno");
        exit(1);
    }
    printf("soft-rt\n");

    running = 1;
    pthread_create(&rtthread, NULL, sampler, NULL);
    ref_deg=25;
    
    double input;
    char mode;
    
    while (ref_deg >-1) {
        ///printf("enter reference \n");
	
	scanf("%c",&mode);
	if (mode == 'r') { // Change angle ref in degrees
	    scanf("%lf",&input);
	    if (input > 15 && input < 88) {
		ref_deg = input;
	    }
	} else if (mode == 'd') { // Add to angle measurement (disturbance)
	    scanf("%lf",&input);
	    if (input > -30 && input < 30) {
		dis_deg = input;
	    }	  
	} else if (mode == 'x') { // Stop program
	    ref_deg = -1;
	} else {
	    // Do nothing
	}
	  
		
    }
    running = 0;
    printf("Exiting\n");

    pthread_join(rtthread, NULL);
    rt_task_delete(rttask);
    return 0;

}

